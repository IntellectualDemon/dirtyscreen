$stdout.sync = true
APP_ROOT = File.dirname(__FILE__)

require './app'
require './backend'
use Dirtyscreen::Backend
require 'rack/ssl-enforcer'
use Rack::SslEnforcer

run Dirtyscreen::App
