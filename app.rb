require 'sinatra/base'
require 'erb'
require 'carrierwave'

module Dirtyscreen
  class Cache
    def self.init()
      @@streamers = []
      @@newshots = []
    end

    def self.streamers()
      @@streamers
    end

    def self.newshots()
      @@newshots
    end
  end

  class App < Sinatra::Base
    configure do
      Cache::init()
      FileUtils.rm_rf("#{APP_ROOT}/tmp/screenshots/.", secure: true)
    end

    get '/' do
      erb :index, locals: { streamers: Cache::streamers() }
    end
    get '/streamer' do
      erb :streamer, locals: { noscroll: true }
    end
    get '/viewer' do
      erb :viewer, locals: { noscroll: true }
    end

    CarrierWave.configure do |config|
      config.cache_dir = 'tmp/cache'
      config.root = APP_ROOT
      config.storage = :file
      config.store_dir = 'tmp/screenshots'
    end

    get '/tmp/screenshots/:wsid' do
      halt(404, "Can't find file") unless /^\d{1,16}$/.match params[:wsid]
      file = File.expand_path("tmp/screenshots/#{params[:wsid]}", APP_ROOT)
      halt(404, "Can't find file") unless File.exist?(file)
      content_type :png
      send_file(file)
    end

    post "/upload/:wsid/:uuid" do
      halt(400, "Can't upload file") unless
        /^\d{1,16}$/.match params[:wsid] and /^[a-f0-9]{8}-([a-f0-9]{4}-){3}[a-f0-9]{12}$/.match params[:uuid]
      if Cache::newshots()[ Integer(params[:wsid]) ].eql? params[:uuid]
        params[:screenshot][:filename] = params[:wsid]
        uploader.store!(params[:screenshot])
        Cache::newshots().delete_at Integer(params[:wsid])
      end
    end

    def uploader
      @uploader ||= CarrierWave::Uploader::Base.new
    end
  end
end
