require 'faye/websocket'
Faye::WebSocket.load_adapter 'thin'
require 'json'
require 'carrierwave'
require "securerandom"

module Dirtyscreen
  class Backend
    Streamer = Struct.new :ws, :viewers
    Viewer = Struct.new :id, :streamers

    def initialize app
      @app = app
      @viewers = []
    end

    def call(env)
      req = env["REQUEST_URI"]
      if Faye::WebSocket.websocket? env and ["/streamer", "/viewer"].include? req
        ws = Faye::WebSocket.new(env, nil, { ping: 30 })

        ws.on :open do |event|
          p [:open, ws.object_id]
          if req.eql? "/streamer"
            Cache::streamers() << Streamer.new(ws, [])
            ws.send("{\"streamer_id\": #{ws.object_id}}")
          end
          @viewers << Viewer.new(ws.object_id, []) if req.eql? "/viewer"
        end

        ws.on :message do |event|
          begin
            data = JSON.parse(event.data)
            p [ :message, data["sessionDescription"]["type"] ] if data["sessionDescription"]
            if data["sessionDescription"] and data["sessionDescription"]["type"].eql? "offer"
              if streamer = Cache::streamers().find {|c| c.ws.object_id.eql? data["id"] }
                streamer.viewers << ws
                data["id"] = ws.object_id
                streamer.ws.send(JSON.generate data)
              end
            elsif data["sessionDescription"] and data["sessionDescription"]["type"].eql? "answer"
              streamer = Cache::streamers().find {|c| c.ws.eql? ws }
              viewer_ws = streamer.viewers.find {|c| c.object_id.eql? data["id"] }
              @viewers.find {|c| c.id.eql? viewer_ws.object_id }.streamers << ws.object_id
              data["id"] = ws.object_id
              viewer_ws.send(JSON.generate data) if viewer_ws
            elsif data["req_upload"].eql? "screenshot" #rate limit
              uuid = SecureRandom.uuid
              Cache::newshots()[ws.object_id] = uuid
              ws.send("{\"screenshot_id\": \"#{uuid}\"}")
            else
              p data
            end
          rescue Exception => e
            p e #logging
          end
        end

        ws.on :close do |event|
          p [:close, ws.object_id, event.code, event.reason]
          Cache::streamers().delete_if {|c| c.ws.eql? ws }
          viewer = @viewers.find {|c| c.id.eql? ws.object_id }
          viewer.streamers.each do |streamer|
            s = Cache::streamers().find {|c| c.ws.object_id.eql? streamer }
            s.viewers.delete_if {|c| c.object_id.eql? ws.object_id } if s
          end if viewer
          FileUtils.rm_f File.expand_path("tmp/screenshots/#{ws.object_id}", APP_ROOT)
          ws = nil
        end

        ws.rack_response
      else
        @app.call(env)
      end
    end

  end
end
