/**
* @author Intellectual Demon <intellectualdemon@protonmail.ch>
**/

var viewer_video = $("#viewer > video"),
    streamer_video = $("#streamer > video"),
    msg_viewer = $("#viewer > .msg"),
    msg_streamer = $("#streamer > .msg"),

    ws, stream, streamer_id, viewers = {}, streamer,
    PING_DROPOUT = 5, //seconds

    wsuri = "wss://inde.server:3000/";

// WebRTC polyfill
RTCPeerConnection =
  window.RTCPeerConnection ||
  window.webkitRTCPeerConnection ||
  window.mozRTCPeerConnection;
RTCSessionDescription =
  window.RTCSessionDescription ||
  window.webkitRTCSessionDescription ||
  window.mozRTCSessionDescription;

// getUserMedia polyfill
window.MediaStream =
  window.MediaStream ||
  window.webkitMediaStream;
var polyGUM = function(constraints) {
  var getUserMedia =
    navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia;
  if(!getUserMedia) {
    return Promise.reject(new Error('getUserMedia is not implemented in this browser'))
  }
  return new Promise(function(resolve, reject) {
    getUserMedia.call(navigator, constraints, resolve, reject)
  });
}
if(navigator.mediaDevices === undefined) navigator.mediaDevices = {};
if(navigator.mediaDevices.getUserMedia === undefined) navigator.mediaDevices.getUserMedia = polyGUM;

// Launch websocket to server
function get_ws(uri) {
  return new Promise(function (resolve, reject) {
    ws = new WebSocket(uri);
    ws.onopen = resolve;
    ws.onmessage = function(m) {
      var data = JSON.parse(m.data);
      if ("streamer_id" in data) streamer_id = data.streamer_id;
      else if ("screenshot_id" in data) post_screenshot(data.screenshot_id);
      else if (data.sessionDescription.type === "offer") streamer_rtc(data).catch(handle_errors);
      else if (data.sessionDescription.type === "answer") {
        streamer.pc.setRemoteDescription(new RTCSessionDescription(data.sessionDescription))
      }
    };
    ws.onerror = function () {};
    ws.onclose = function () {}
  })
}

// Handle streamer answering call.
function streamer_rtc (odesc) {
  var pc = new RTCPeerConnection(
    {"iceServers": [{ "urls": "stun:stun.l.google.com:19302" }]},
    { optional: [{ "RtpDataChannels": false }] }
  );
  var id = parseInt(odesc.id);
  viewers[id] = { pc: pc, dc: null, ping: { id: null, limit: PING_DROPOUT, dropped: 0 } };
  odesc = odesc.sessionDescription;
  pc.onicecandidate = function (e) {
    if (e.candidate === null) ws.send( JSON.stringify({ id: id, sessionDescription: pc.localDescription }) )
  };
  pc.addStream(stream);
  pc.ondatachannel = function (e) { dc_init(viewers[id].dc = e.channel || e, id) };
  return new Promise(function (resolve, reject) {
    pc.setRemoteDescription(new RTCSessionDescription(odesc), function () {
      pc.createAnswer( function (adesc) {
        pc.setLocalDescription(adesc, resolve, reject)
      }, reject)
    }, reject)
  }).catch(function (e) {
    delete viewers[id];
    $(".viewer_count > span").text(Object.keys(viewers).length);
    handle_errors(e)
  })
}

// Handle viewer initiating call
function viewer_rtc() {
  var pc = new RTCPeerConnection(
    {"iceServers": [{ "urls": "stun:stun.l.google.com:19302" }]},
    { optional: [{ "RtpDataChannels": false }] }
  );
  streamer = { pc: pc, dc: null, ping: { id: null, limit: PING_DROPOUT, dropped: 0 } };
  return new Promise(function (resolve, reject) {
    pc.onicecandidate = function (e) {
      if (e.candidate === null) get_ws(wsuri + "viewer").then(function () {
        ws.send( JSON.stringify({ id: parseInt( window.location.search.substring(1) ), sessionDescription: pc.localDescription }) )
      })
    };
    if ("ontrack" in pc) pc.ontrack = function (e) {
      viewer_video.attr("src", window.URL.createObjectURL(e.streams[0]))
    };
    else if ("onaddstream" in pc) pc.onaddstream = function (e) {
      viewer_video.attr("src", window.URL.createObjectURL(e.stream))
    };
    dc_init(streamer.dc = pc.createDataChannel('webcam', {reliable: true}));
    pc.createOffer(function (desc) {
      pc.setLocalDescription(desc, resolve, reject)
    }, reject, { offerToReceiveAudio: 1, offerToReceiveVideo: 1 })
  }).catch(handle_errors);
}

// Set up data channel for heartbeat.
function dc_init(dc, id) {
  var ping = id ? viewers[id].ping : streamer.ping;

  function gen_ping_id() {
    var arr = new Uint32Array(4);
    window.crypto.getRandomValues(arr);
    ping.id = Array.prototype.slice.call(arr).map(function(byte) { return byte.toString(16) }).join('')
  }

  function dropped_pc(iv) {
    clearInterval(iv);
    if (id) {
      viewers[id].pc.close();
      delete viewers[id];
      $(".viewer_count > span").text(Object.keys(viewers).length);
    } else {
      streamer.pc.close();
      streamer = null;
      $(".streamer_online > button").css({background: ""})
    }
  }

  dc.onopen = function () {
    if (id) $(".viewer_count > span").text(Object.keys(viewers).length);
    else $(".streamer_online > button").css({background: "lime"});
    var iv = setInterval(function () {
      !ping.id ? (ping.dropped = 0) : ping.limit - ping.dropped ? ping.dropped++ : dropped_pc(iv);
      gen_ping_id();
      dc.readyState === "open" ?
        dc.send(JSON.stringify({ type: "ping", id: ping.id })) :
        ping.dropped = ping.limit;
    }, 1000)
  };

  dc.onmessage = function (e) {
    var data = JSON.parse(e.data);
    if (data.type === "ping") {
      data.type = "pong";
      dc.readyState === "open" && dc.send(JSON.stringify(data))
    } else if (data.type === "pong" && data.id === ping.id) ping.id = null
  };

  dc.onclose = function () {}
}

// Initialize streamer video, stats, and screenshot service.
var screenShotIv;

function streamer_init() {
  get_ws(wsuri + "streamer")
    .then(function () { return navigator.mediaDevices.getUserMedia({video: true, audio: true}) })
    .then(function(s) {
      screenShotIv = setInterval(request_screenshot, 30000);
      streamer_video.attr("src", window.URL.createObjectURL(stream = s));
      streamer_video.on("playing", request_screenshot);
      $(".viewer_count > span").text(Object.keys(viewers).length)
    }).catch(handle_errors)
}

// Regularly update streamer's screenshot.
function request_screenshot() {
  if (ws.readyState === ws.OPEN) ws.send("{\"req_upload\": \"screenshot\"}");
  else clearInterval(screenShotIv)
}

function post_screenshot(uuid) {
  var canvas = document.createElement("canvas"),
      dim = Math.min(streamer_video[0].videoWidth, streamer_video[0].videoHeight),
      long = streamer_video[0].videoWidth > streamer_video[0].videoHeight
  canvas.width = dim;
  canvas.height = dim;
  canvas.getContext('2d').drawImage(
    streamer_video[0],
    long ? (streamer_video[0].videoWidth - dim)/2 : 0,
    long ? 0 : (streamer_video[0].videoHeight - dim)/2,
    dim, dim, 0, 0, dim, dim
  );
  var fd = new FormData();
  canvas.toBlob(function (ssBlob) {
    fd.append("screenshot", ssBlob);
    $.ajax({ url: "upload/" + streamer_id + "/" + uuid, type: "POST", data: fd, processData: false, contentType: false })
  }, 'image/png');
}

// Navigation
$(document).on("pagebeforeshow", "#streamer", streamer_init);
$(document).on("pagebeforeshow", "#viewer", viewer_rtc);
$("#browse a").click(function (e) {
  this.href="viewer?" + e.currentTarget.dataset.streamer
});
$(window).resize(function () {
  $("video").width($(window).width());
  $("video").height($(window).height())
});
$(window).resize();
$(document).scroll(function () {
  var op = $("body").scrollTop();
  if (op < 200) {
    if ($(".ui-footer").is(":hidden")) $(".ui-footer").show();
    $(".ui-footer").css("opacity", 1-op/200);
  } else if (op > 200 && $(".ui-footer").is(":visible")) $(".ui-footer").hide()
});
$(".streamer_online > button").click(function () { streamer ? streamer.dc.close() : viewer_rtc() })

// Error handling
function handle_errors(e) {
  console.log(e.name + ": " + e.message)
}
